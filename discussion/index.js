// console.log("Hello, b190");

// if statement
/*
	- executes a statement if a specified condition is true
	- can stand alone without the else statement

	Syntax:
		if(condition){
			statement/code block
		}
*/

let numA = -1;

if(numA < 0){
	console.log("Hello");
}

console.log(numA < 0);

if(numA > 0){
	console.log("This statement will not be printed!");
}

let city = "New York"

if(city === "New York"){
	console.log("Welcome to New York City!");
}

// else if clause
/*
	- execute a statement if previous conditions are false and if the specified condition is true
	- "else if" clause is optional and this can be added to capture additional conditions to change the flow of a program
*/

let numB = 1;

if (numA > 0){
	console.log("Hello");
} else if (numB > 0) {
	console.log("World");
}

city = "Tokyo";

if(city === "New York") {
	console.log("Welcome to New York City");
} else if(city === "Tokyo"){
	console.log("Welcome to Tokyo!");
}

// else statement
/*
	- executes the statement if all other conditions are false
	- the "else" statement is also optional and can be added to capture any other result to change the flow of our program
*/

if(numA > 0){
	console.log("Hello");
} else if (numB === 0){
	console.log("World");
} else {
	console.log("Again");
}

// Another example
let age = 20;
if(age <= 18){
	console.log("Not allowed to drink");
} else {
	console.log("Matanda ka na, shot na!")
}

/*
	miniactivity
		create a conditional statement if the height is below 150, display "Did not passed height requirement". If above or equal 150, display "Passed the minimum height requirement"

	** stretch goal:
		put it inside a function
*/

// let height = 151;
// if(height >= 150){
// 	console.log("Passed the minimum height requirement");
// } else {
// 	console.log("Did not passed height requirement");
// }

// function reqHeight(height){
// 	if(height >= 150){
// 		console.log("Passed the minimum height requirement");
// 	} else {
// 		console.log("Did not passed height requirement");
// 	}
// }
// let height = prompt("Enter your height in cm:");
// reqHeight(height);

let message = "No message";
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if (windSpeed < 30){
		return "Not a typhoon yet."
	} else if (windSpeed <= 61){
		return "Tropical Depression detected."
	} else if (windSpeed >= 62 && windSpeed <= 88){
		return "Tropical Strom detected."
	} else if (windSpeed >= 89 && windSpeed <= 117){
		return "Severe Tropical Strom detected."
	} else {
		return "Typhoon detected."
	}
}

message = determineTyphoonIntensity(69);
console.log(message);

if (message == "Tropical Strom detected."){
	console.warn(message);
}

// Truthy and Falsy
/*
	- In JS, a truthy value is a value that is considered true when encountered in Bollean context.
	- Values are considered true unless defined otherwise

	Falsy values / exceptions for truthy
	1. false
	2. 0
	3. -0
	4. ""
	5. null
	6. undefined
	7. NaN
*/

// Truthy Examples
if(true){
	console.log("Truthy!");
}

if(1){
	console.log("Truthy!");
}

if([]){
	console.log("Truthy!");
}

// Falsy Examples
if(false){
	console.log("Falsy!");
}

if(0){
	console.log("Falsy!");
}

if(undefined){
	console.log("Falsy!");
}

//Conditional (Ternanry) Operator
/*
	The Ternary operator takes in three operands
	1. condition
	2. expression to execute if the condition is truthy
	3. expression to execute if the condition is falsy

	SYNTAX:
		(condition) ? ifTrue : ifFalse 
*/

// Single Statement Execution

let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult)

let name;

/*function isOfLegalAge(){
	name = "John";
	return "You are of the legal age limit";
}

function isUnderAge(){
	name = "Jane";
	return "You are under the legal age limit";
}

let ageAge = parseInt(prompt("what is your age?"));
console.log(ageAge);
console.log(typeof ageAge)
let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ", " + name);*/

// Switch Statement
/*
	SYNTAX:
		switch(expression){
			case value
				statement;
				break;
			default:
				statement;
				break;
		}

*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day)

	switch (day){
		case "monday":
			console.log("The color of the day is red");
			break;
		case "tuesday":
			console.log("The color of the day is orange");
			break;
		case "wednesday":
			console.log("The color of the day is yellow");
			break;
		case "thursday":
			console.log("The color of the day is green");
			break;
		case "friday":
			console.log("The color of the day is blue");
			break;
		case "saturday":
			console.log("The color of the day is indigo");
			break;
		/*case ("saturday" || "sunday"):
			console.log("The color of the day is violet");
			break;*/
		case "sunday":
			console.log("The color of the day is violet");
			break;
		default:
			console.log("Please input a valid day!")
			break;
	}

// Try-Catch-Fnally Statement
function showIntensityAlert(windSpeed){
	try {
		// attempt to execute a code
		alerat(determineTyphoonIntensity(windSpeed))
	}

	catch(error){
		//catch errors within the 'try' statement
		console.log(error);
		console.warn(error.message)
	}

	finally {
		// continue execution of code regardless of success or failure of code execution in the "try" to handle/resolve errors
		alert("Intensity updates will show new alert.")
	}
}

showIntensityAlert(56);