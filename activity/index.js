console.log("Hello World");

// Activity 19.1.1

let username = prompt("What is your username?");
let password = prompt("What is your password?");
let role = prompt("What is your role?").toLowerCase();

function credentials(){
	if ((username == null || username == "") || (password == null || password == "") || (role == null || role == "")) {
		alert("The input must not be empty!"); 
	}
	else{
		switch(role){
			case "admin":
				alert("Welcome back to the class portal, admin!");
				break;
			case "teacher":
				alert("Thank you for logging in, teacher!");
				break;
			case "rookie":
				alert("Welcome to the class portal, student");
				break;
			case "student":
				alert("Welcome to the class portal, student");
				break;
			default:
				alert("Role out of range.")
				break;
		}
	}
}
credentials();

// Activity 19.1.2. 

let score1 = 89;
let score2 = 99;
let score3 = 99;
let score4 = 99;
console.log("checkAverage( " + score1 + ", " + score2 + ", " + score3 + ", " + score4 + " );");

function checkAverage(score1,score2,score3,score4){
	let average = Math.round((score1 + score2 + score3 + score4) / 4);
	if (average <= 74){
		console.log("Hello, student, your average is " + average + ". The letter equivalent is F");
	} else if (average >= 75 && average <=79){
		console.log("Hello, student, your average is " + average + ". The letter equivalent is D");
	} else if (average >= 80 && average <=84){
		console.log("Hello, student, your average is " + average + ". The letter equivalent is C");
	} else if (average >= 85 && average <=89){
		console.log("Hello, student, your average is " + average + ". The letter equivalent is B");
	} else if (average >= 90 && average <=95){
		console.log("Hello, student, your average is " + average + ". The letter equivalent is A");
	} else if (average >= 96){
		console.log("Hello, student, your average is " + average + ". The letter equivalent is A+");
	}
}
checkAverage(score1,score2,score3,score4);